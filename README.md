# CaptchaOfBlockPuzzle

## 介绍
OpenHarmony的JavaScript组件库,实现拖动的拼图验证功能
- 项目名称：CaptchaOfBlockPuzzle
- 项目源地址：https://gitee.com/cheeseCui/captcha-of-block-puzzle
- 所属系列：OpenHarmony下的的JS 自定义组件开发示例
- 开发版本：OpenHarmony-SDK-2.0-Canary , DevEco Studio2.2 Beta1
- Demo来源：《鸿蒙操作系统应用开发实践》


## 效果

 ![screenshots](./screenshots/overview.gif) 
 
 
## 使用说明
### 目录
当前代码目录如下所示：
1.  组件：在entry\src\main\js\default\common\component\blockPuzzle中，包含定义组件的hml、css、js文件
2.  demo: 在entry\src\main\js\default\pages\index中，定义了domo页面，使用了拼图验证组件。

 ![files](./screenshots/files.png) 


### 使用

#### 1.  页面hml引入
 ```
    <element name="blockPuzzleCaptcha"  src="../../common/component/blockPuzzle/blockPuzzle.hml"></element>
 ```

#### 2.  在页面hml中使用组件：
绑定组件的属性、图片和事件
 ```
    <blockPuzzleCaptcha  attrs="{{comAttrs}}" img-list="{{imgList}}" @handle-result="handleResult"></blockPuzzleCaptcha>
 ```

#### 3.  页面的JS：
在页面的js中定义组件的属性、图片和绑定的事件：

##### props

| 属性名称           |  值类型   | 描述                                                         |
| ---------------- | ------- | ------------------------------------------------------------ |
| img-list         | Array   | 组件拼图使用的图片，必填                                                    |
| com-attrs        | Object  | 组件属性： 里面包含compWidth组件的宽度（即图片的宽度）、imgHeight拼图的高度（注意与宽度比例对应）、cutSquareWidth拼图方块的边长       |
     

##### Events

| 名称             | 描述                                                         |
| --------------- | ------------------------------------------------------------ |
| handle-result   | 自定义的点击事件调用方法，默认参数为null，用于接收组件执行成功或失败的返回数据     |


代码示例：
 ```
    export default {
        data: {
            // 图片list
            imgList:[
                '/common/images/verifyImgs/1.jpg',
                '/common/images/verifyImgs/2.jpg',
                '/common/images/verifyImgs/3.jpg',
                '/common/images/verifyImgs/4.jpg',
                '/common/images/verifyImgs/5.jpg',
                '/common/images/verifyImgs/6.jpg',
                '/common/images/verifyImgs/7.jpg',
                '/common/images/verifyImgs/8.jpg',
                '/common/images/verifyImgs/9.jpg',
                '/common/images/verifyImgs/10.jpg',
                '/common/images/verifyImgs/12.jpg'
            ],
            // 组件属性
            comAttrs:{
                // 组件的宽度 单位为px，也为图片的宽度 必填
                compWidth:300 ,
                // 拼图 图片的高度：注意要与实际比例对应  必填
                imgHeight: 150,
                // 拼图块 中间正方形的大小 px，默认50
                cutSquareWidth: 40
            },
        },
        onInit() {
    
        },
        onShow(){
        },
        /**
         * 拖动拼图结果 的处理
         * e ：包含 result 拼图结果: fail/success
         * e ：包含 pointsInfo 数组： 滑动触点的位置信息
         * e ：包含 slideTimeLong 毫秒数：滑动的持续时间
         */
        handleResult(e){
            console.log("父组件接收结果： "+ JSON.stringify(e.detail));
        }
    }
 ```

#### 4. 组件给页面传值：
在组件的拼图结束，无论成功或失败都会通过自定义事件 handle-result 给父页面传递数据

 ```
    /**
     * 给父组件传值
     * @param result 拼图的结果：fail or success
     * @param slideTimeLong 滑动持续时间
     */
    emitParent(result,slideTimeLong) {
        this.$emit('handleResult', {
            result: result, // 拼图结果
            pointsInfo: this.movePoints, // 拖动轨迹
            slideTimeLong: slideTimeLong // 滑动时间 毫秒
        })
    },
 ```
##### 数据介绍：

| 属性名称           |  值类型   | 描述                                                         |
| ---------------- | ------- | ------------------------------------------------------------ |
| result           | String  | 拼图的结果：success 或 fail                                                   |
| pointsInfo       | Array   | 拖动过程的手势点数组       |
| slideTimeLong    | Number  | 拖动过程的持续时间：毫秒       |
   
