export default {
    props: {
        // 图片列表
        imgList: Array,
        // 组件属性
        attrs: Object
    },
    data: {
        // 控件的总宽度
        comWidth: 0,
        // 完整图片的高度，注意实际图片的比例
        imgHeight: 0,

        // 拖动的比率：拼图/滑块 = 总宽度-拼图宽度*2/总宽度-滑块宽度*2
        ratio: 0,

        // 拼图图片url
        imgUrl: '',
        // 图片index 使用过的数组
        imgIndexArr: [],

        // 拼图的top
        picTop: 0,
        // 拼图的移动距离: 根据滑块的移动距离及比率进行计算
        picMoveX: 0,
        // 拼图块 中间正方形 的宽度
        picCutWidth: 50,
        // 缺失拼图的，left
        cutLeft: 0,
        // 拼图块 上面和左面圆的背景图片属性
        picItemCircleAttr: {
            width: 25, // 拼图圆形 的尺寸 直径
            itemIntersectWidth: 2, // 与方块交汇重叠的 宽度
            picTopCircleBgLeft: 0, // 拼图块 上面圆 的背景图 left 值
            picTopCircleBgTop: 0, // 拼图块 上面圆 的背景图 top 值
            picRightCircleBgLeft: 0, // 拼图块 右侧圆 的背景图 left值
            picRightCircleBgTop: 0, // 拼图块 右侧圆 的背景图 top值
        },
        // 拼图快 上面和左侧 原的位置：上面 top值 和 右侧的right值
        picItemCircleOverPosition: 0,

        // 滑块内文字
        sliderText: '',
        // 滑块按钮位置left
        sliderBtnLeft: 0,
        // 滑块按钮的宽度
        sliderBtnWidth: 40,
        // 滑动按钮点击效果
        slicerBtnActive: '',
        // timeout 计时
        timer: null,
        // 滑块禁启用
        sliderDisable: false,

        // 拼的结果 0-原始状态；1-拖拽；2-成功；3-失败
        resultState: 0,
        // 滑块结束 左侧样式
        resultMaskClass: '',
        // 滑块结束 按钮样式
        resultBtnClass: '',

        // 行为点数组数据 -- 可用于 识别是否 非法侵入
        movePoints: [],
        // 滑动开始时间
        slideStartTimestamp: 0,
        // 滑动结束时间
        slideEndTimestamp: 0,
    },
    onInit() {
        // 赋值组件的宽度
        this.comWidth = this.attrs.compWidth;
        // 组件高度
        this.imgHeight = this.attrs.imgHeight;

        // 拼图块 中间正方形的大小 px，默认50
        this.picCutWidth = this.attrs.cutSquareWidth ? this.attrs.cutSquareWidth : 50;

        // 拼图 圆的宽度
        this.picItemCircleAttr.width = this.picCutWidth / 3;

        // 拼图 圆的位置
        this.picItemCircleOverPosition = this.picItemCircleAttr.width - this.picItemCircleAttr.itemIntersectWidth;

        // 比率
        this.ratio = (this.comWidth - (this.picCutWidth + this.picItemCircleAttr.width - this.picItemCircleAttr.itemIntersectWidth)) / (this.comWidth - this.sliderBtnWidth);

        // 初始化
        this.initState();
    },

    /**
     * 初始化 所有状态
     */
    initState() {
        // 初始滑块的文字
        this.sliderText= '向右滑动填充拼图'
        this.slideStartTimestamp = 0;

        // 随机获取图片 url
        this.radomImgUrl();

        // pic 拼图的移动距离
        this.picMoveX = 0;
        // 计算 拼图的 top
        let cutAllWidth = this.picCutWidth + this.picItemCircleAttr.width;
        this.picTop = (Math.random() * (this.imgHeight - cutAllWidth - 10) + this.picItemCircleAttr.width);
        // 计算 拼图的 left
        this.cutLeft = (Math.random() * (this.comWidth - 2 * cutAllWidth)) + cutAllWidth;

        // 计算 拼图块 上面圆点位置
        let picTopCircleBgLeft = -(this.cutLeft + (this.picCutWidth - this.picItemCircleAttr.width) / 2)
        let picTopCircleBgTop = -this.picTop + (this.picItemCircleAttr.width - this.picItemCircleAttr.itemIntersectWidth);
        this.picItemCircleAttr.picTopCircleBgLeft = picTopCircleBgLeft
        this.picItemCircleAttr.picTopCircleBgTop = picTopCircleBgTop

        // 计算拼图宽 左侧圆点的位置
        let picRightCircleBgLeft = -this.cutLeft - (this.picCutWidth - this.picItemCircleAttr.itemIntersectWidth)
        let picRightCircleBgTop = -this.picTop - (this.picCutWidth - this.picItemCircleAttr.width) / 2;
        this.picItemCircleAttr.picRightCircleBgLeft = picRightCircleBgLeft
        this.picItemCircleAttr.picRightCircleBgTop = picRightCircleBgTop

        // 原始状态
        this.resultState = 0
        this.resultMaskClass = ''
        this.resultBtnClass = ''

        // 滑块的位置归 1
        this.sliderBtnLeft = 0
        // 按钮去掉点击效果
        this.slicerBtnActive = ''
        // 启用 slider
        this.sliderDisable = false
    },

    /**
     * 随机或图片url:在图片未全部使用过的情况下不重复，尽量让图片资源多些
     */
    radomImgUrl() {
        // imgIndex: 随机获取图片
        let imgIndex = Math.round(Math.random() * (this.imgList.length - 1));
        // 如果 imgIndexArr 中没有当前 imgIndex
        if (this.imgIndexArr.indexOf(imgIndex) < 0) {
            // push 进入数组
            this.imgIndexArr.push(imgIndex)
            this.imgUrl = this.imgList[imgIndex]
        }
        // 如果 数组中已存在 当前index
        else {
            // 如果 数组长度 和 图片list 长度一样，说明 所有图片已经 展示过
            if (this.imgIndexArr.length == this.imgList.length) {
                // 清空数组
                this.imgIndexArr = [];
            }
            // 重新回调 生成随机数
            this.radomImgUrl();
        }
    },

    /**
     * 拖动滑块
     */
    handleSliderStart(){
        // 获取开始时间
        this.slideStartTimestamp = this.getTimestamp();

        // 滑块内的文字 清空
        this.sliderText = ''
    },

    /**
     * 拖动滑块
     */
    handleSliderMove(e) {
        // 将 点信息 放入数组
        this.movePoints.push(e.touches[0])

        // 按钮点击效果
        this.slicerBtnActive = 'slider-btn-active';
        // x 距离
        let posX = e.touches[0].localX;


        // 拖拽起点终点
        if (posX <= 0 || posX >= (this.comWidth - this.sliderBtnWidth)) {
            return;
        }
        // 滑块位置
        this.sliderBtnLeft = e.touches[0].localX
        // 拼图块的 位置
        this.picMoveX = this.sliderBtnLeft * this.ratio
    },

    /**
     * 拖动滑块 开
     */
    handleSliderEnd(e) {
        let that = this;
        let result = '';

        // 获取开始时间
        this.slideEndTimestamp = this.getTimestamp();
        // 获取开始到结束时间的差值 毫秒
        let slideTimeLong = this.slideEndTimestamp-this.slideStartTimestamp;

        // 提示 对错
        if (this.picMoveX <= this.cutLeft + 5 && this.sliderBtnLeft >= this.cutLeft - 5) {
            console.log('子组件：成功');

            // 将 点信息 放入数组
            this.movePoints.push(e.touches[0])

            // 结果
            result = 'success'

            // 状态成功
            this.resultState = 2
            this.resultMaskClass = 'slider-mask_success'
            this.resultBtnClass = 'slider-btn_success'
        }
        else {
            console.log('子组件：失败');

            // 清空点数组
            this.movePoints = []

            // 结果
            result = 'fail'

            // 状态 错号
            this.resultState = 3
            this.resultMaskClass = 'slider-mask_fail'
            this.resultBtnClass = 'slider-btn_fail'

            // 几秒后 归位+刷新
            this.timmer = setTimeout(() => {
                // 重新 初始
                that.initState();
                that.timer = null;
            }, 1000)
        }
        // 禁用slider
        this.sliderDisable = true

        // 调用父组件传值
        this.emitParent(result,slideTimeLong);
    },

    /**
     * 获取当前时间毫秒
     */
    getTimestamp(){
        let curDate = new Date();
        return curDate;
    },

    /**
     * 给父组件传值
     * @param result 拼图的结果：fail or success
     * @param slideTimeLong 滑动持续时间
     */
    emitParent(result,slideTimeLong) {
        this.$emit('handleResult', {
            result: result, // 拼图结果
            pointsInfo: this.movePoints, // 拖动轨迹
            slideTimeLong: slideTimeLong // 滑动时间 毫秒
        })
    },

    /**
     * 刷新
     */
    handleRefresh() {
        this.initState()
    }
}
