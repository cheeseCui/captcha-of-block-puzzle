export default {
    data: {
        // 图片list
        imgList:[
            '/common/images/verifyImgs/1.jpg',
            '/common/images/verifyImgs/2.jpg',
            '/common/images/verifyImgs/3.jpg',
            '/common/images/verifyImgs/4.jpg',
            '/common/images/verifyImgs/5.jpg',
            '/common/images/verifyImgs/6.jpg',
            '/common/images/verifyImgs/7.jpg',
            '/common/images/verifyImgs/8.jpg',
            '/common/images/verifyImgs/9.jpg',
            '/common/images/verifyImgs/10.jpg',
            '/common/images/verifyImgs/12.jpg'
        ],
        // 组件属性
        comAttrs:{
            // 组件的宽度 单位为px，也为图片的宽度 必填
            compWidth:300 ,
            // 拼图 图片的高度：注意要与实际比例对应  必填
            imgHeight: 150,
            // 拼图块 中间正方形的大小 px，默认50
            cutSquareWidth: 40
        },
    },
    onInit() {

    },
    onShow(){
    },
    /**
     * 拖动拼图结果 的处理
     * e ：包含 result 拼图结果: fail/success
     * e ：包含 pointsInfo 数组： 滑动触点的位置信息
     * e ：包含 slideTimeLong 毫秒数：滑动的持续时间
     */
    handleResult(e){
        console.log("父组件接收结果： "+ JSON.stringify(e.detail));
    }
}
